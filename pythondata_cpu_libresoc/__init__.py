import os.path
__dir__ = os.path.split(os.path.abspath(os.path.realpath(__file__)))[0]
data_location = os.path.join(__dir__, "verilog")
src = "https://git.libre-soc.org/?p=soc.git"

# Module version
version_str = "0.0.post661"
version_tuple = (0, 0, 661)
try:
    from packaging.version import Version as V
    pversion = V("0.0.post661")
except ImportError:
    pass

# Data version info
data_version_str = "0.0.post608"
data_version_tuple = (0, 0, 608)
try:
    from packaging.version import Version as V
    pdata_version = V("0.0.post608")
except ImportError:
    pass
data_git_hash = "f4cdf933f0345a8af4a643f82e00ebec036b3f3b"
data_git_describe = "24jan2021_ls180-526-gf4cdf933"
data_git_msg = """\
commit f4cdf933f0345a8af4a643f82e00ebec036b3f3b
Author: Luke Kenneth Casson Leighton <lkcl@lkcl.net>
Date:   Sun Apr 18 11:18:59 2021 +0100

    update README
"""

# Tool version info
tool_version_str = "0.0.post53"
tool_version_tuple = (0, 0, 53)
try:
    from packaging.version import Version as V
    ptool_version = V("0.0.post53")
except ImportError:
    pass


def data_file(f):
    """Get absolute path for file inside pythondata_cpu_libresoc."""
    fn = os.path.join(data_location, f)
    fn = os.path.abspath(fn)
    if not os.path.exists(fn):
        raise IOError("File {f} doesn't exist in "
                      "pythondata_cpu_libresoc".format(f))
    return fn
