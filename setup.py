import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

from pythondata_cpu_libresoc import version_str

setuptools.setup(
    name="pythondata-cpu-libresoc",
    version=version_str,
    author="Luke Kenneth Casson Leighton",
    author_email="lkcl@libre-soc.org",
    description="""\
Python module containing verilog files for the Libre-SOC OpenPOWER cpu.""",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://git.libre-soc.org/pythondata-cpu-libresoc",
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU Lesser General Public License v3 or later (LGPLv3+)",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.5',
    zip_safe=False,
    packages=setuptools.find_packages(),
    package_data={
    	'cpu_libresoc': ['cpu_libresoc/verilog/**'],
    },
    include_package_data=True,
    project_urls={
        "Bug Tracker": "https://bugs.libre-soc.org",
        "Source Code": "https://git.libre-soc.org/pythondata-cpu-libresoc",
    },
)
