# pythondata-cpu-libresoc

Non-Python  files needed for the cpu Libre-SOC, packaged
into a Python module so they can be used with Python libraries and tools.

This Useful for usage with tools like
[LiteX](https://github.com/enjoy-digital/litex.git).

The data files can be found under the Python module `pythondata_cpu_libresoc`. The
`pythondata_cpu_libresoc.data_location` value can be used to find the files on the file
system.

Example of getting the data file directly;
```python
import pythondata_cpu_libresoc

my_data_file = "abc.txt"

with open(os.path.join(pythondata_cpu_libresoc.data_location, my_data_file)) as f:
    print(f.read())
```

Example of getting the data file using `litex.data.find` API;
```python
from pythondata_cpu_libresoc import data_file

my_data_file = "abc.txt"

with open(data_file(my_data_file)) as f:
    print(f.read())
```

The verilog files are built from https://git.libre-soc.org/soc.git
due to the original source being over 130,000 lines of nmigen.
Instructions on how to re-build the verilog can be found in the
[Software prerequisites](https://libre-soc.org/HDL_workflow/#software-prerequisites)
section of Libre-SOC [HDL_workflow](https://libre-soc.org/HDL_workflow/)


## Installing from git repository

## Manually

You can install the package manually, however this is **not** recommended.

```
git clone https://github.com/litex-hub/pythondata-cpu-libresoc.git
cd pythondata-cpu-libresoc
sudo python setup.py install
```

## Using [pip](https://pip.pypa.io/) with git repository

You can use [pip](https://pip.pypa.io/) to install the data package directly
from github using;

```
pip install --user git+https://github.com/litex-hub/pythondata-cpu-libresoc.git
```

If you want to install for the whole system rather than just the current user,
you need to remove the `--user` argument and run as sudo like so;

```
sudo pip install git+https://github.com/litex-hub/pythondata-cpu-libresoc.git
```

You can install a specific revision of the repository using;
```
pip install --user git+https://github.com/litex-hub/pythondata-cpu-libresoc.git@<tag>
pip install --user git+https://github.com/litex-hub/pythondata-cpu-libresoc.git@<branch>
pip install --user git+https://github.com/litex-hub/pythondata-cpu-libresoc.git@<hash>
```

### With `requirements.txt` file

Add to your Python `requirements.txt` file using;
```
-e git+https://github.com/litex-hub/pythondata-cpu-libresoc.git
```

To use a specific revision of the repository, use the following;
```
-e https://github.com/litex-hub/pythondata-cpu-libresoc.git@<hash>
```

## Installing from [PyPi](https://pypi.org/project/pythondata-cpu-libresoc/)

## Using [pip](https://pip.pypa.io/)

```
pip install --user pythondata-cpu-libresoc
```
